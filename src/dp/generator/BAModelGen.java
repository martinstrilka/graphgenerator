/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphException;
import dp.graph.Graph;
import dp.graph.GraphUndirected;
import java.util.ArrayList;
import java.util.List;

/**
 * Tato třída generuje bezškálový neorientovaný graf podle modelu
 * preferenčního připojování Albert-Barabasi.
 * Generátor pracuje v exponenciálním čase O(n^2) kde n je počet vrcholů grafu
 * @author Martin
 */
public class BAModelGen extends AbstractGenerator {

    /**
     * Slouží pouze ke zrychlení, obsahuje předpočítané capability * degree
     */
    protected float[] probabilityHelper;
    /**
     * Suma (capability[i] * degree[i]) přes všechny vrcholy i = 0,..., i = numOfVertexes
     */
    protected float sumOfDegAndCap;
    /**
     * Počet vložených hran v průběhu generování
     */
    protected int insertedVertexes;

    /**
     * Konstruktor
     */
    public BAModelGen() {
        super();
    }

    /**
     * Tato metoda implementuje model preferenčního připojování Albert-Barabasi,
     * generuje bezškálový neorientovaný graf,
     * začíná se kompletním grafem o třech vrcholech, pak se postupně každý
     * nový vrchol připojí k 2 náhodně vybraným vrcholům v grafu
     * pravděpodobnost že se připojí novy uzel na starý uzel <b>v</b> je
     * (k / suma k všech vrcholů grafu)
     * k... stupen vrcholu <b>v</b>
     * @param numOfVertexes počet vrcholů grafu
     * @return graph vygenerovaný bezškálový graf
     * @throws Exception
     */
    public GraphUndirected genGraph(int numOfVertexes) throws GraphException {
        graphUndirected = new GraphUndirected(numOfVertexes);
        probabilityHelper = new float[numOfVertexes];
        sumOfDegAndCap = 0f;
        // začínáme kompletním grafem na 3 vrcholech
        insertFirst3Vertexes(numOfVertexes, false);

        // vkládání ostatních vrcholů
        for (int i = 3; i < numOfVertexes; i++) {
            int vertex1 = getVertexToLink();
            int vertex2 = getVertexToLink(vertex1);
            linkUndirected(vertex1, i);
            linkUndirected(vertex2, i);
            insertedVertexes++;

            sumOfDegAndCap -= probabilityHelper[vertex1];
            sumOfDegAndCap -= probabilityHelper[vertex2];
            probabilityHelper[i] = graphUndirected.getDegrees()[i] * graphUndirected.getCapabilities()[i];
            probabilityHelper[vertex1] = graphUndirected.getDegrees()[vertex1] * graphUndirected.getCapabilities()[vertex1];
            probabilityHelper[vertex2] = graphUndirected.getDegrees()[vertex2] * graphUndirected.getCapabilities()[vertex2];

            sumOfDegAndCap += probabilityHelper[i];
            sumOfDegAndCap += probabilityHelper[vertex1];
            sumOfDegAndCap += probabilityHelper[vertex2];
        }
        return graphUndirected;
    }
    /**
     * Tato metoda funguje stejně jako {@see graphGen} s tím rozdílem, že
     * parametr numOfConnectedVertexes představuje počet vrcholů ke kterým se připojí
     * nový vrchol
     * @param numOfVertexes - celkový počet vrcholů grafu
     * @param numOfConnectedVertexes - počet vrcholů, ke kterým se připojuje nový vrchol
     * @return graf
     * @throws GraphException
     */
    public GraphUndirected genGraphWithMoreConnectionsSlow(int numOfVertexes, int numOfConnectedVertexes) throws GraphException {
        graphUndirected = new GraphUndirected(numOfVertexes);
        probabilityHelper = new float[numOfVertexes];
        sumOfDegAndCap = 0f;
        // začínáme kompletním grafem na 3 vrcholech
        insertFirst3Vertexes(numOfVertexes, false);
        // vkládání ostatních vrcholů
        insertOthers(numOfVertexes, numOfConnectedVertexes);
        return graphUndirected;
    }
    /**
     * Pravděpodobnost že se připojí novy uzel na starý uzel <b>v</b> je
     * (k*n / suma k*n všech vrcholů grafu)
     * k... stupen vrcholu <b>v</b>
     * n... zdatnost vrcholu <b>v</b>
     * @param numOfVertexes
     * @param numOfConnectedVertexes
     * @return
     * @throws GraphException
     */
    public GraphUndirected genGraphWithCapabilitySlow(int numOfVertexes, int numOfConnectedVertexes) throws GraphException {
        graphUndirected = new GraphUndirected(numOfVertexes, 2);
        probabilityHelper = new float[numOfVertexes];
        sumOfDegAndCap = 0f;
        // začínáme kompletním grafem na 3 vrcholech
        insertFirst3Vertexes(numOfVertexes, true);
        // vkládání ostatních vrcholů
        insertOthers(numOfVertexes, numOfConnectedVertexes);
        return graphUndirected;
    }

    protected void insertFirst3Vertexes(int numOfVertexes, boolean includeCapability) throws GraphException {
        linkUndirected(0, 1);
        linkUndirected(1, 2);
        linkUndirected(2, 0);
        if (includeCapability) {
            graphUndirected.setCapabilities(generateNumbersFromExponentialDistribution(-alpha, numOfVertexes));
        } else {
            graphUndirected.setCapabilities(new float[numOfVertexes]);
            for (int i= 0; i < numOfVertexes; i++) {
                graphUndirected.getCapabilities()[i] = 1;
            }
        }
        for (int i = 0; i < 3; i++) {
            probabilityHelper[i] = graphUndirected.getDegrees()[i] * graphUndirected.getCapabilities()[i];
            sumOfDegAndCap += probabilityHelper[i];
        }
        insertedVertexes = 3;
    }

    private void insertOthers(int numOfVertexes, int numOfConnectedVertexes) throws GraphException {
        for (int i = 3; i < numOfVertexes; i++) {
            List<Integer> vertexesToConnect = new ArrayList<>();
            for (int j = 0; j < numOfConnectedVertexes; j++) {
                if (insertedVertexes <= j) {
                    break;
                }
                int vertex = getVertexToLink();
                while (vertexesToConnect.contains(vertex)) {
                    vertex = getVertexToLink();
                }
                vertexesToConnect.add(vertex);
            }
            for (Integer vertex : vertexesToConnect) {
                linkUndirected(vertex, i);
                sumOfDegAndCap -= probabilityHelper[vertex];
                probabilityHelper[i] = ((GraphUndirected)graphUndirected).getCapabilities()[i];
                probabilityHelper[vertex] = graphUndirected.getCapabilities()[vertex];
                probabilityHelper[i] = graphUndirected.getDegrees()[i] * graphUndirected.getCapabilities()[i];
                probabilityHelper[vertex] = graphUndirected.getDegrees()[vertex] * graphUndirected.getCapabilities()[vertex];
                sumOfDegAndCap += probabilityHelper[i];
                sumOfDegAndCap += probabilityHelper[vertex];
            }
            insertedVertexes++;
        }
    }

    protected int getVertexToLink() {
        float randNum = (float)random.nextDouble() * sumOfDegAndCap;
        float increment = 0f;
        for (int i = 0; i < insertedVertexes; i++) {
            increment += probabilityHelper[i];
            if (randNum < increment) {
                return i;
            }
        }
        return 0;
    }
    /**
     * Náhodně vybere vrchol, kromě vrcholu s indexem except
     * pravděpodobnost náhodně vybraného vrcholu p = degree * capability / sumOfDegAndCap
     * @param except vrchol, který nemůže být vybrán
     * @return index vybraného vrcholu
     */
    protected int getVertexToLink(int except) {
        float sum = sumOfDegAndCap;
        if (except != -1) {
             sum -= probabilityHelper[except];
        }
        float randNum = (float)random.nextDouble() * sum;
        float increment = 0f;
        for (int i = 0; i < insertedVertexes; i++) {
            if (i == except)
                continue;
            increment += probabilityHelper[i];
            if (randNum < increment) {
                return i;
            }
        }
        return 0;
    }
}
