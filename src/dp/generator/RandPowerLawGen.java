/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphException;
import dp.graph.Graph;
import dp.graph.GraphUndirected;
import java.util.List;
//import nl.peterbloem.powerlaws.Continuous;

/**
 * Tato třída umožňuje generova graf s 
 * @author Martin
 */
public class RandPowerLawGen extends BAModelGen {

    public RandPowerLawGen() {
        super();
    }

    public GraphUndirected generateGraphPowerLaws(int numOfVertexes, int numOfEdges, float alpha) throws GraphException {
        this.alpha = alpha;
        graphUndirected = new GraphUndirected(numOfVertexes);
        //List<Double> list = new Continuous(2, -alpha).generate(numOfEdges);
        //float[] arr = new float[numOfEdges];
        //for (int i= 0; i < list.size(); i++) {
        //    arr[i] = (float)list.get(i).doubleValue();
        //}
        graphUndirected.setCapabilities(generateNumbersFromExponentialDistribution(-alpha, numOfVertexes));
        //graph.setCapabilities(arr);
        probabilityHelper = graphUndirected.getCapabilities();
        insertedVertexes = numOfVertexes;
        sumOfDegAndCap = 0;
        for (int i = 0; i < numOfVertexes; i++) {
            sumOfDegAndCap += graphUndirected.getCapabilities()[i];
        }
        for (int i = 0; i < numOfEdges; i++) {
            int vertex1 = getVertexToLink();
            int vertex2 = getVertexToLink(vertex1);
            linkUndirected(vertex1, vertex2);
        }
        return graphUndirected;
    }
}
