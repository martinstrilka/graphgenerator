/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphDirected;
import dp.graph.GraphException;

/**
 *
 * @author Martin
 */
public class CopyingModel extends AbstractGenerator {

    private float p = 0.4f;
    private int insertedVertexes = 0;

    public CopyingModel(float p) {
        super();
        this.p = p;
    }

    public GraphDirected genGraph(int numOfVertexes, int outDegree) throws GraphException {
        graphDirected = new GraphDirected(numOfVertexes);
        linkDirected(0,1);
        linkDirected(1,0);
        linkDirected(0,2);
        linkDirected(2,0);
        linkDirected(2,1);
        linkDirected(1,2);
        insertedVertexes = 3;
        for (int i = 3; i < numOfVertexes; i++) {
            int prototype = getVertexToLink();
            for (int j = 0; j < outDegree; j++) {
                if (random.nextDouble() < p) {
                    linkDirected(i, getVertexToLink());
                } else {
                    linkDirected(i, graphDirected.getData()[prototype][j]);
                }
            }
            insertedVertexes++;
        }
        return graphDirected;
    }

    @Override
    protected int getVertexToLink() {
        return random.nextInt(insertedVertexes);
    }

    @Override
    protected int getVertexToLink(int except) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
