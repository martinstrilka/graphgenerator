/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphException;
import dp.graph.Graph;
import dp.graph.GraphUndirected;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Martin
 */
public class BAModelParalelGen extends BAModelGen {

    public BAModelParalelGen() {
        super();
    }

    public Graph generateGraphParalel(int numOfVertexes, boolean capability) throws GraphException {
        graphUndirected = new GraphUndirected(numOfVertexes);
        probabilityHelper = new float[numOfVertexes];

        // začínáme kompletním grafem na 3 vrcholech
        linkUndirected(0, 1);
        linkUndirected(1, 2);
        linkUndirected(2, 0);
        if (capability) {
            ((GraphUndirected)graphUndirected).setCapabilities(generateNumbersFromExponentialDistribution(-alpha, numOfVertexes));
        } else {
            ((GraphUndirected)graphUndirected).setCapabilities(new float[numOfVertexes]);
            for (int i= 0; i < numOfVertexes; i++) {
                ((GraphUndirected)graphUndirected).getCapabilities()[i] = 1;
            }
        }
        for (int i = 0; i < 3; i++) {
           probabilityHelper[i] = graphUndirected.getDegrees()[i] * ((GraphUndirected)graphUndirected).getCapabilities()[i];
           sumOfDegAndCap += probabilityHelper[i];
        }
        insertedVertexes = 3;
        // tři vrcholy vloženy do grafu

        Thread thread1 = new Thread(new Runnable() {

            @Override
            public void run() {
                t1Insert(3, 2);
            }
        });
        Thread thread2 = new Thread(new Runnable() {

            @Override
            public void run() {
                t2Insert(4, 2);
            }
        });
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread1.join();
        } catch (InterruptedException e) {
            throw new GraphException(e.getMessage());
        }
        return graphUndirected;
    }

    private final Lock lock = new ReentrantLock();
    private final Condition prvniVklada = lock.newCondition();
    private final Condition druhyVklada = lock.newCondition();

    private void t1Insert(int beginning, int increment) {
        for (int i = beginning; i < graphUndirected.getNumOfVertexes(); i += increment) {
            int vertex1 = getVertexToLink();
            int vertex2 = getVertexToLink(vertex1);
            //System.out.println("t1 - after finding");
            lock.lock();
            try {
                while (insertedVertexes % 2 == 0) {
                    try {
                        // prvni vklada sudy vrchol
                        druhyVklada.await();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                //System.out.println("t1");
                try {
                    linkUndirected(vertex1, i);
                    linkUndirected(vertex2, i);
                } catch (GraphException ex) {}

                updateProbability(i, vertex1, vertex2);
                //System.out.println(Thread.currentThread().getName() + ": inserted point " + i);
                prvniVklada.signal();
            }
            finally {
                lock.unlock();
            }
        }
    }

    private void t2Insert(int beginning, int increment) {
        for (int i = beginning; i < graphUndirected.getNumOfVertexes(); i += increment) {
            int vertex1 = getVertexToLink();
            int vertex2 = getVertexToLink(vertex1);
            //System.out.println("t2 - after finding");
            lock.lock();
            try {
                while (insertedVertexes % 2 == 1) {
                    // druhy vklada lichy vrchol
                    try {
                        prvniVklada.await();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                //System.out.println("t2");
                try {
                    linkUndirected(vertex1, i);
                    linkUndirected(vertex2, i);
                } catch (GraphException ex) {
                }
                updateProbability(i, vertex1, vertex2);
                //System.out.println(Thread.currentThread().getName() + ": inserted point " + i);
                druhyVklada.signal();
            } finally {
                lock.unlock();
            }
        }

    }

    private void updateProbability(int actual, int vertex1, int vertex2) {
        sumOfDegAndCap -= probabilityHelper[vertex1];
        sumOfDegAndCap -= probabilityHelper[vertex2];
        probabilityHelper[actual] = graphUndirected.getDegrees()[actual] * ((GraphUndirected)graphUndirected).getCapabilities()[actual];
        probabilityHelper[vertex1] = graphUndirected.getDegrees()[vertex1] * ((GraphUndirected)graphUndirected).getCapabilities()[vertex1];
        probabilityHelper[vertex2] = graphUndirected.getDegrees()[vertex2] * ((GraphUndirected)graphUndirected).getCapabilities()[vertex2];
        sumOfDegAndCap += probabilityHelper[actual];
        sumOfDegAndCap += probabilityHelper[vertex1];
        sumOfDegAndCap += probabilityHelper[vertex2];
        insertedVertexes++;
    }
}
