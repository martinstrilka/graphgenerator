/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphException;
import dp.graph.Graph;
import dp.graph.GraphUndirected;

/**
 * Tato třída generuje bezškálový neorientovaný graf podle modelu
 * preferenčního připojování Albert-Barabasi.
 * Generátor pracuje v lineárním čase O(n) kde n je počet vrcholů grafu
 * @author Martin
 */
public class BAModelGenQuick extends AbstractGenerator {

    /**
     * Toto pole slouží pro určení pravděpodobnosti s jakou se nový vrchol
     * připojí k jiným vrcholů v grafu. Každý vrchol je v poli zapsán tolikrát
     * jak má velký stupeň.
     * Délka pole je rovna (2 * numOfVertexes - 3) * 2
     */
    private int[] probabilityField;
    /**
     * Index v poli probabilityField
     */
    private int probabilityFieldIndex;

    /**
     * Konstruktor
     */
    public BAModelGenQuick() {
        super();
    }

    /**
     * Při vybírání vrcholů, ke kterým se má nový vrchol připojit se neprochází
     * sekvenčně všechny stávající vrcholy, ale každý vrchol má v poli tolik
     * buněk jak má velký stupeň. Z tohoto pole se pak náhodně vyberou dva indexy,
     * na kterých jsou zapsány ID vrcholů ke kterým se bude nový vrchol připojovat.
     * @param numOfVertexes
     * @return
     * @throws GraphException
     */
    public GraphUndirected genGraph(int numOfVertexes) throws GraphException {
        graphUndirected = new GraphUndirected(numOfVertexes);
        probabilityField = new int[(2 * numOfVertexes - 3) * 2];
        
        linkUndirected(0, 1);
        linkUndirected(1, 2);
        linkUndirected(2, 0);
        probabilityField[0] = 0;
        probabilityField[1] = 0;
        probabilityField[2] = 1;
        probabilityField[3] = 1;
        probabilityField[4] = 2;
        probabilityField[5] = 2;
        probabilityFieldIndex = 6;

        for (int i = 3; i < numOfVertexes; i++) {
            int vertex1 = getVertexToLink();
            int vertex2 = getVertexToLink(vertex1);
            linkUndirected(vertex1, i);
            linkUndirected(vertex2, i);
            probabilityField[probabilityFieldIndex++] = vertex1;
            probabilityField[probabilityFieldIndex++] = vertex2;
            probabilityField[probabilityFieldIndex++] = i;
            probabilityField[probabilityFieldIndex++] = i;
        }
        return graphUndirected;
    }

    protected int getVertexToLink() {
        return probabilityField[random.nextInt(probabilityFieldIndex)];
    }

    protected int getVertexToLink(int except) {
        int index = probabilityField[random.nextInt(probabilityFieldIndex)];
        while (index == except) {
            index = probabilityField[random.nextInt(probabilityFieldIndex)];
        }
        return index;
    }

}
