/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.generator;

import dp.graph.GraphException;
import dp.graph.Graph;
import dp.graph.GraphDirected;
import dp.graph.GraphUndirected;
import java.util.Random;

/**
 *
 * @author Martin
 */
public abstract class AbstractGenerator {

    /**
     * Generovaný graf
     */
    protected GraphUndirected graphUndirected;
    /**
     * Generovaný graf
     */
    protected GraphDirected graphDirected;
    /**
     * Random
     */
    protected Random random;
    /**
     * exponent pro generátor čísel z mocniného rozdělení
     */
    protected float alpha = 2.5f;

    /**
     * Konstruktor
     */
    public AbstractGenerator() {
        random = new Random();
    }

    /**
     * Propojí dva uzly obosměrnou vazbou
     * @param v1 index prvního vrcholu
     * @param v2 index druhého vrcholu
     * @throws Exception Pokud jsou dva spojované vrcholy totožné
     */
    protected void linkUndirected(int v1, int v2) throws GraphException {
        if (v1 == v2) {
            throw new GraphException("Can not link vertex to itself!");
        }
        if (graphUndirected.getDegrees()[v1] == graphUndirected.getData()[v1].length) {
            int[] arr = graphUndirected.getData()[v1];
            graphUndirected.getData()[v1] = new int[graphUndirected.getData()[v1].length * 2];
            System.arraycopy(arr, 0, graphUndirected.getData()[v1], 0, arr.length);
        }

        if (graphUndirected.getDegrees()[v2] == graphUndirected.getData()[v2].length) {
            int[] arr = graphUndirected.getData()[v2];
            graphUndirected.getData()[v2] = new int[graphUndirected.getData()[v2].length * 2];
            System.arraycopy(arr, 0, graphUndirected.getData()[v2], 0, arr.length);
        }

        graphUndirected.getData()[v1][graphUndirected.getDegrees()[v1]++] = v2;
        graphUndirected.getData()[v2][graphUndirected.getDegrees()[v2]++] = v1;
    }
    /**
     * Propojí dva uzly jednosměrnou vazbou v1 => v2
     * @param v1 index prvního vrcholu
     * @param v2 index druhého vrcholu
     * @throws Exception Pokud jsou dva spojované vrcholy totožné
     */
    protected void linkDirected(int v1, int v2) throws GraphException {
        if (v1 == v2) {
            throw new GraphException("Can not link vertex to itself!");
        }
        if (graphDirected.getDegrees()[v1] == graphDirected.getData()[v1].length) {
            int[] arr = graphDirected.getData()[v1];
            graphDirected.getData()[v1] = new int[graphDirected.getData()[v1].length * 2];
            System.arraycopy(arr, 0, graphDirected.getData()[v1], 0, arr.length);
        }
        graphDirected.getData()[v1][graphDirected.getDegrees()[v1]++] = v2;
        graphDirected.getInDegree()[v2]++;
    }
    /**
     * Náhodně vybere vrchol
     * pravděpodobnost, že bude vrchol vybrán je p = degree * capability / sumOfDegAndCap
     * @return index vybraného vrcholu
     */
    protected abstract int getVertexToLink();
    /**
     * Náhodně vybere vrchol, kromě vrcholu s indexem except
     * pravděpodobnost náhodně vybraného vrcholu p = degree * capability / sumOfDegAndCap
     * @param except vrchol, který nemůže být vybrán
     * @return index vybraného vrcholu
     */
    protected abstract int getVertexToLink(int except);
    /**
     * Vygeneruje řadu čísel pocházejíchích z mocniného rozdělení
     * @param k exponent
     * @param count počet vygenerovaných čísel
     * @return pole vygenerovaných čísel
     */
    protected float[] generateNumbersFromExponentialDistribution(double k, int count) {
        float[] arr = new float[count];
        for (int i = 0; i < count; i++) {
            arr[i] = (float)(Math.log(random.nextDouble()) / k);
            //arr[i] = (float)Math.pow(1 - random.nextDouble(), -1 / (k - 1));
        }
        return arr;
    }
}
