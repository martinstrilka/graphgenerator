/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp;

import dp.graph.Graph;
import dp.graph.GraphException;
import dp.generator.BAModelGen;
import dp.generator.BAModelGenQuick;
import dp.generator.CopyingModel;
import dp.generator.RandPowerLawGen;
import dp.graph.GraphDirected;
import java.util.Random;

import jsc.independentsamples.SmirnovTest;

/**
 *
 * @author Martin
 */
public class Main {
    
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws GraphException {
        /*
        //GraphGenerator generator = new GraphGenerator();
        //ScaleFreeGraph graph = generator.generateGraphParalel(300000, false);
        //ScaleFreeGraph graph = generator.generateGraph(10000, true, 2);
        //ScaleFreeGraph graph = generator.generateGraphPowerLaws(100, 800, 3f);
        //Graph graph = new BAModelGenQuick().genGraph(100000);
        //Graph graph = new RandPowerLawGen().generateGraphPowerLaws(1000, 2000, 2.5f);
        GraphDirected graph = new CopyingModel(0.5f).genGraph(100, 2);
        //graph.printGraphInfo();
        graph.printGraphComplex();
        //graph.saveCumulativeDistbition();
        //graph.printGraphSimple();
        */
        double[] d =  {
            78.000000,
            10.000000,
            4.000000,
            2.000000,
            2.000000,
            1.666667,
            1.333333,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000};
        double[] d2 = { 
            73.000000,
            15.000000,
            5.000000,
            3.000000,
            2.333333,
            1.666667,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000,
            1.000000};
        double[] c = {
        78,
        10,
        4,
        2,
        2,
        1,
        1,
        1,
        1
        };

        double[] c2 = {
            73,
            15,
            5,
            3,
            1,
            1,
            1,
            1
        };
        SmirnovTest test = new SmirnovTest(d, d2);
        System.out.println(test.getSP());
    }
    
    public static int[] generateNumbersFromExponentialDistribution(double k, int maxValue, int count) {
        Random random = new Random();
        int[] arr = new int[count];
        double sumOfIntervals = 0;
        double[] intervals = new double[maxValue + 1];
        for (int i = 1; i < intervals.length; i++) {
            intervals[i] = Math.pow((double)i , k);
            sumOfIntervals += intervals[i];
        }
        double rand;
        double sum;
        for (int i = 0; i < count; i++) {
            while ((rand = random.nextDouble() * sumOfIntervals) >= sumOfIntervals) {}
            sum = 0;
            for (int j = 1; j < intervals.length; j++) {
                sum += intervals[j];
                if (rand < sum) {
                    arr[i] = j;
                    break;
                }
            }
        }
        return arr;
    }
}
