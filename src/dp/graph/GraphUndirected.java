/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.graph;

import java.util.Arrays;

/**
 * Jedná se o neorientovaný graf, ke kterému je přidaná zdatnos z BA modelu
 * @author Martin
 */
public class GraphUndirected extends Graph {

    /**
     * V tomto poli je uložena zdatnost každého vrcholu,
     * indexy odpovídají indexům v matici data
     */
    private float[] capability;

     /**
     * Vrátí pole se zdatnosí vrcholů
     * @return
     */
    public float[] getCapabilities() {
        return capability;
    }
    /**
     * Nastaví pole se zdatností jedinců na value
     * @param value
     */
    public void setCapabilities(float[] value) {
        capability = value;
    }

    /**
     *
     * @param numOfVertexes
     */
    public GraphUndirected(int numOfVertexes, int startDegree) {
        super(numOfVertexes, startDegree);
        capability = new float[numOfVertexes];
    }
    /**
     *
     * @param numOfVertexes
     */
    public GraphUndirected(int numOfVertexes) {
        this(numOfVertexes, 2);
    }

    /**
     * Pro každý vrchol vytiskne [zdatnost][stupeň]: [sousední vrcholy]
     */
    public void printGraphComplex() {
        for (int i = 0; i < numOfVertexes; i++) {
            System.out.print(i + " [" + capability[i] + "][" + degreeOfVertex[i] + "]: ");
            for (int j = 0; j < degreeOfVertex[i]; j++) {
                System.out.print(data[i][j] + ", ");
            }
            System.out.println("");
        }
    }
    /**
     * Vytiskne
     * <ul>
     *  <li>počet vrcholů</li>
     *  <li>počet hran</li>
     *  <li>největší stupeň</li>
     *  <li>exponent alpha - zjištěn pomocí MLE</li>
     * </ul>
     */
    public void printGraphInfo() {
        int sumOfLinks = 0;
        double alpha = 0;
        double xMin = 1d;
        int maxDegree = 0;
        for (int i = 0; i < degreeOfVertex.length; i++) {
            sumOfLinks += degreeOfVertex[i];
            alpha += Math.log(degreeOfVertex[i] / xMin);
            if (maxDegree < degreeOfVertex[i]) {
                maxDegree = degreeOfVertex[i];
            }
        }
        alpha = Math.pow(alpha, -1) * numOfVertexes;
        alpha += 1;

        System.out.println(
                "number of vertexes: " + numOfVertexes
                + "\nnumber of links: " + sumOfLinks/2
                + "\nmax degree: " + maxDegree
                + "\nMLE - alpha: " + alpha);

        float minCapability = Integer.MAX_VALUE;
        float maxCapability = Integer.MIN_VALUE;
        for (int i= 0; i < numOfVertexes; i++) {
            if (minCapability > capability[i]) {
                minCapability = capability[i];
            }
            if (maxCapability < capability[i]) {
                maxCapability = capability[i];
            }
        }
        System.out.printf("minCapability: %f\nmaxCapability: %f\n", minCapability, maxCapability);

        Arrays.sort(degreeOfVertex);
        for (int i = degreeOfVertex.length - 1; i > degreeOfVertex.length - 10; i--) {
            System.out.println(degreeOfVertex[i]);
        }
        /*
        System.out.println("-----------------------------");
        Arrays.sort(capability);
        for (int i = capability.length - 1; i > capability.length - 20; i--) {
            System.out.println(capability[i]);
        }
        */
    }
}
