/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dp.graph;

/**
 *
 * @author Martin
 */
public class GraphDirected extends Graph {
    
    private int[] inDegree;

    public int[] getInDegree() {
        return inDegree;
    }
    
    public GraphDirected(int numOfVertexes) {
        super(numOfVertexes);
        inDegree = new int[numOfVertexes];
    }

    /**
     * Pro každý vrchol vytiskne [vstupní stupeň][výstupní stupeň]: [sousední vrcholy]
     */
    @Override
    public void printGraphComplex() {
        for (int i = 0; i < numOfVertexes; i++) {
            System.out.print(i + " [" + inDegree[i] + "][" + degreeOfVertex[i] + "]: ");
            for (int j = 0; j < degreeOfVertex[i]; j++) {
                System.out.print(data[i][j] + ", ");
            }
            System.out.println("");
        }
    }

    @Override
    public void printGraphInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
