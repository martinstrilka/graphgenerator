
package dp.graph;

/**
 * Třída vyjímek vyhazovaných v balíčku graph
 *
 * @author Martin
 */
public class GraphException extends Exception {

    /**
     * Konstruktor
     * @param message
     */
    public GraphException(String message) {
        super(message);
    }
}
