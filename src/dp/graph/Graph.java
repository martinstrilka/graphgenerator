
package dp.graph;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tato třída reprezentuje bezškálový graf
 *
 * @author Martin
 */
public abstract class Graph {
    /**
     * V teto matici je uložen graf, každý vrchol je ve vlastním řádku,
     * sloupce jsou vazby na ostatní vrcholy, zapsány jsou jen indexy sousedních
     * vrcholů, každý řádek proto může být nestejně dlouhý, defaultní délka je 2
     */
    protected int[][] data;
    /**
     * V tomto poli jsou uloženy stupně každého vrhcholu,
     * indexy odpovídají indexům v matici data
     */
    protected int[] degreeOfVertex;
    /**
     * Počet vrcholů v grafu
     */
    protected int numOfVertexes;

    /**
     * Vrátí matici data
     * @return matice data
     */
    public int[][] getData() {
        return data;
    }
    /**
     * Vrátí pole se stupni vrcholů
     * @return
     */
    public int[] getDegrees() {
        return degreeOfVertex;
    }
    /**
     * Vrátí počet vertexů
     * @return
     */
    public int getNumOfVertexes() {
        return numOfVertexes;
    }

    /**
     * Konstruktor
     * @param numOfVertexes - počet vrcholů grafu
     */
    public Graph(int numOfVertexes) {
        this(numOfVertexes, 2);
    }
    /**
     * Konstruktor
     * @param numOfVertexes - počet vrcholů grafu
     * @param startDegree - alokovaný stupeň vrcholu (slouží hlavně pro optimalizaci)
     */
    public Graph(int numOfVertexes, int startDegree) {
        this.numOfVertexes = numOfVertexes;
        data = new int[numOfVertexes][startDegree];
        degreeOfVertex = new int[numOfVertexes];
    }

    /**
     * TODO
     */
    public void saveGraph() {
    }
    /**
     * Uloží kumulativní funkci stupňů vrcholů, pro každý stupeň je spočítán
     * počet vrcholů s daným stupněm, zapisují se pouze počty vrcholů s daným
     * stupněm od nejnizšího, každý stupeň je na novém řádku, pokud daný
     * stupeň nemá žádný vrchol, tak je zapsán pouze znak nového řádku.
     */
    public void saveCumulativeDistbition() throws GraphException {
        StringBuilder sb = new StringBuilder();
        int maxDegree = 0;
        for (int i = 0; i < numOfVertexes; i++) {
            if (maxDegree < degreeOfVertex[i]) {
                maxDegree = degreeOfVertex[i];
            }
        }
        for (int i = 1; i <= maxDegree; i++) {
            int count = 0;
            for (int j = 0; j < numOfVertexes; j++) {
                if (degreeOfVertex[j] == i) {
                    count++;
                }
            }
            if (count != 0) {
                sb.append(count);
                sb.append("\n");
            } else {
                sb.append("\n");
            }
        }
        save("degrees.txt", sb.toString());
    }
    /**
     * Totéž co funkce <b>saveCumulativeDistbition()</b> s rozdílem, že
     * jsou sloučeny intervaly stupňů podle mocniny dvou. To znamená, že velikosti
     * intervalů jsou následující 2, 4, 8, 16, 32, ... , 2^30
     */
    public void saveCumulativeDistributionWithIntervals() throws GraphException {
        StringBuilder sb = new StringBuilder();
        List<Integer> listOfDegrees = new ArrayList();
        int last = 0;
        for (int stupen = 1; stupen < 29; stupen++) {
            int intervalLength2 = (int) Math.pow(2, stupen);
            int count = 0;
            for (int i = 0; i < numOfVertexes; i++) {
                if (degreeOfVertex[i] > last && degreeOfVertex[i] <= last + intervalLength2) {
                    count++;
                }
            }
            listOfDegrees.add(count);
            last += intervalLength2;
            if (count != 0) {
                sb.append(count);
            }
            sb.append("\n");
        }
        save("degreesIntervals.txt", sb.toString());
    }
    /**
     * Vytiskne pouze stupně vrcholů, na jeden řádek jeden stupeň
     */
    public void printGraphSimple() {
        for (int i = 0; i < numOfVertexes; i++) {
            System.out.println(degreeOfVertex[i]);
        }
    }
    /**
     * Pro každý vrchol vytiskne [zdatnost][stupeň]: [sousední vrcholy]
     */
    public abstract void printGraphComplex();
    /**
     * Vytiskne
     * <ul>
     *  <li>počet vrcholů</li>
     *  <li>počet hran</li>
     *  <li>největší stupeň</li>
     *  <li>exponent alpha - zjištěn pomocí MLE</li>
     * </ul>
     */
    public abstract void printGraphInfo();
    /**
     * Uloží {@param text} do souboru s názvem a cestou path
     * @param path - jméno a cesta souboru
     * @param text - data souboru
     */
    private void save(String path, String text) throws GraphException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(path, false);
            writer.write(text);
        } catch (Exception e) {
            throw new GraphException(e.getMessage());
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
